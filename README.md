# Kafka on Docker

A short tutorial on how to set up Kafka using Docker and produce data in shell or via a python script.

### Docker set-up
Clone this repository and open the docker-compose file.
In this file, two services are defined: Zookeeper and Kafka.

Please change the `KAFKA_ADVERTISED_HOST_NAME` to your host IP; the IP on your main network adaptor.

Now, run the following command in the terminal:
```
docker-compose up --scale kafka=3 -d
```
This will start the docker containers in detached mode, with 3 Kafka instances, which is a good number to get started. Feel free to change the number of Kafka instances.

### Shell
#### Kafka producer
To start a docker container serving as a Kafka producer, run the following command:
```
docker run --name kafka-producer --rm -v /var/run/docker.sock:/var/run/docker.sock -e HOST_IP=10.0.1.173 -i -t wurstmeister/kafka /bin/bash
```
Please, make sure to use your own IP address as the `HOST_IP` variable.

Now, create a topic to send messages to with the following command:
```
opt/kafka/bin/kafka-topics.sh --create --topic test-topic --partitions 1 --replication-factor 2 --bootstrap-server `broker-list.sh`
```
Feel free to change the number of partitions or replicas to spread the data over the different brokers. Preferably, the replication factor is at least set to 2, so you won't lose data when 1 broker fails.

To check if the topic was correctly created, run the describe command:
```
opt/kafka/bin/kafka-topics.sh --describe --topic test-topic --bootstrap-server `broker-list.sh`
```

Now, start producing to 'test-topic':
```
opt/kafka/bin/kafka-console-producer.sh --topic=test-topic --broker-list=`broker-list.sh`
```
After executing this command, you will see a line starting with '>'. This indicates that you are producing. You can now type a message and press ENTER, to send a message to the Kafka brokers.

To stop consuming, press CTRL + C. To exit the docker container, enter `exit`.

If you want to delete a topic, run the following command:
```
opt/kafka/bin/kafka-topics.sh --delete --topic test-topic --bootstrap-server `broker-list.sh`
```

#### Kafka consumer
Open a new terminal and start a docker container, serving as the Kafka consumer, with the following command:
```
docker run --name kafka-consumer --rm -v /var/run/docker.sock:/var/run/docker.sock -e HOST_IP=10.0.1.173 -i -t wurstmeister/kafka /bin/bash
```
Please, make sure to use your own IP address as the HOST_IP variable.

Start consuming the 'test-topic', by executing the following command:
```
opt/kafka/bin/kafka-console-consumer.sh --topic=test-topic --from-beginning --bootstrap-server `broker-list.sh`
```
The tag `--from-beginning` will read all data that has been produced to the topic, even before the consumer connected to the Kafka broker. Removing this tag will only read new data.

To stop consuming, press CTRL + C. To exit the docker container, enter `exit`.

#### Testing fault-tolerance
One of the key features of Kafka's distributed architecture is its fault-tolerance. Even if one of the brokers fails, all data will be preserved and producers and consumers will be able to continue sending and receiving data. Provided that the replication factor is set at least to 2, of course.

To test this, make sure Kafka is running with 3 brokers, as specified earlier in this tutorial. Then open a producer and a consumer container.

In the producer instance, run the following command to create a topic with 3 partitions and a replication factor of 2:
```
opt/kafka/bin/kafka-topics.sh --create --topic fault-tolerance --partitions 3 --replication-factor 2 --bootstrap-server `broker-list.sh`
```

Now start producing and consuming data with the following commands:
```
opt/kafka/bin/kafka-console-producer.sh --topic=fault-tolerance --broker-list=`broker-list.sh`
```
```
opt/kafka/bin/kafka-console-consumer.sh --topic=fault-tolerance --from-beginning --bootstrap-server `broker-list.sh`
```
After sending and receiving some messages, stop producing and consuming by pressing CTRL + C, but do not exit the containers.

Let's take a look at how the data is partitioned and replicated across the different brokers:
```
opt/kafka/bin/kafka-topics.sh --describe --topic fault-tolerance --bootstrap-server `broker-list.sh`
```

As defined in a previous command, the topic is split into three partitions. Each partition is stored on two brokers, of which one is the leader.  

| Topic: fault-tolerance | PartitionCount: 3 | ReplicationFactor: 2 | Configs: segment.bytes=1073741824 |  |  
| ---------------------- | ----------------- | -------------------- | ------ | --------------------------- |  
| Topic: fault-tolerance | Partition: 0 | Leader: 1003 | Replicas: 1003,1002 | Isr: 1003,1002 |  
| Topic: fault-tolerance | Partition: 1 | Leader: 1002 | Replicas: 1002,1001 | Isr: 1002,1001 |  
| Topic: fault-tolerance | Partition: 2 | Leader: 1001 | Replicas: 1001,1003 | Isr: 1001,1003 |  

In a separate terminal, run the `docker ps` command to see the list of running containers. Choose one Kafka container and stop it with the `docker stop <container>` command.

Re-run the describe command to see how the distribution of the data across brokers has changed.

In my case, the second partition now has a different leader. Zookeeper regularly sends heartbeats requests to the brokers, so it will notice that a broker has failed. Since broker 1002 is not available anymore, Zookeeper elects broker 1001 as the new leader, since this broker is currently live and has an in-sync replica of that partition. 

| Topic: fault-tolerance | PartitionCount: 3 | ReplicationFactor: 2 | Configs: segment.bytes=1073741824 |  |  
| ---------------------- | ----------------- | -------------------- | ------ | --------------------------- |  
| Topic: fault-tolerance | Partition: 0 | Leader: 1003 | Replicas: 1003,1002 | Isr: 1003 |  
| Topic: fault-tolerance | Partition: 1 | Leader: 1001 | Replicas: 1002,1001 | Isr: 1001 |  
| Topic: fault-tolerance | Partition: 2 | Leader: 1001 | Replicas: 1001,1003 | Isr: 1001,1003 |  

If you consume the topic from the beginning, you will see that all data is still available.

You could stop one more container, to see that from this point, the system will not be able to restore itself. An error message will be displayed when trying to consume the topic and the description of the topic will show a partition with a missing leader, since both brokers that had a replica of that partition have failed. The one remaining broker is now the leader of both partitions that are stored on that broker.

| Topic: fault-tolerance | PartitionCount: 3 | ReplicationFactor: 2 | Configs: segment.bytes=1073741824 |  |  
| ---------------------- | ----------------- | -------------------- | ------ | --------------------------- |  
| Topic: fault-tolerance | Partition: 0 | Leader: 1003 | Replicas: 1003,1002 | Isr: 1003 |  
| Topic: fault-tolerance | Partition: 1 | Leader: none | Replicas: 1002,1001 | Isr: 1001 |  
| Topic: fault-tolerance | Partition: 2 | Leader: 1003 | Replicas: 1001,1003 | Isr: 1003 |  

### Python
#### Set-up
Before being able to produce to and consume from a topic with python scripts, we are going to create a new topic and make sure the Python scripts connect to the correct ports.

Start a Docker container using the following command:
```
docker run --name kafka-broker --rm -v /var/run/docker.sock:/var/run/docker.sock -e HOST_IP=10.0.1.173 -i -t wurstmeister/kafka /bin/bash
```

Next, create the 'Orders' topic:
```
opt/kafka/bin/kafka-topics.sh --create --topic orders --partitions 3 --replication-factor 2 --bootstrap-server `broker-list.sh`
```
Feel free to change the number of partitions or replicas to spread the data over the different brokers in a different way.

Exit the shell by entering `exit`.

Now, we need to know the ports of the Kafka containers, so we can connect to them in the python scripts.
To know the ports on which the docker containers are accessible, run the `docker ps` command.

In my case, the following ports are listed:
> PORTS  
> 0.0.0.0:32796->9092/tcp  
> 0.0.0.0:32797->9092/tcp  
> 0.0.0.0:32798->9092/tcp

Next, update the `bootstrap_servers` argument in both producer.py and consumer.py files with your ports. In my case, this is:
```
bootstrap_servers=['localhost:32796', 'localhost:32797', 'localhost:32798']
```

#### Kafka producer
Run the python script `producer.py`. Every 3 seconds, an order will be generated by picking a random item and quantity.

#### Kafka consumer
Run the python script `consumer.py`. All orders will be read by the consumer and written to a csv file that will continually updated as new orders arrive.

You can experiment with the consumer by stopping the consumer script, while leaving the producer script running, and then restarting the consumer script after some time. You will see that the consumer script will resume consuming the topic where it left off. This is due to the fact that Zookeeper stores the consumer offsets.

### Shut down Kafka
Make sure you have stopped producing and/or consuming, by pressing CTRL + C in the respective terminals and/or exiting the shell by entering `exit`.

Finally, execute the command `docker-compose down`.

To verify all containers have stopped, run the command `docker ps`.