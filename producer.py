# Import packages
from time import sleep
from json import dumps
from kafka import KafkaProducer
import random

# Logging: uncomment to enable debugging
# import logging
# logging.basicConfig(level=logging.DEBUG)

# Initialize Kafka Producer
producer = KafkaProducer(bootstrap_servers=['localhost:32796', 'localhost:32797', 'localhost:32798'],
                         value_serializer=lambda x: dumps(x).encode('utf-8'))

# Generate and send messages
for id in range(100):
    data = {'order_id': id, 'product': random.choice(
        ['milk', 'bread', 'eggs', 'jam']), 'quantity': random.randint(1, 5)}
    producer.send('orders', value=data)
    print("Order ", id, " sent!")
    sleep(3)
