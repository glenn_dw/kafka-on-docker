# Run Kafka Shell in a docker instance - to start a producer
docker run --name docker-producer --rm -v /var/run/docker.sock:/var/run/docker.sock -e HOST_IP=192.168.0.123 -i -t wurstmeister/kafka /bin/bash

KAFKA_HOME = /opt/kafka

# Create 'test-topic'
$KAFKA_HOME/bin/kafka-topics.sh --create --topic test-topic \
--partitions 2 --replication-factor 1 --bootstrap-server `broker-list.sh`

# Describe 'test-topic'
$KAFKA_HOME/bin/kafka-topics.sh --describe --topic test-topic \
--bootstrap-server `broker-list.sh`

# Start 'test-topic' producer
$KAFKA_HOME/bin/kafka-console-producer.sh --topic=test-topic \
--broker-list=`broker-list.sh`

# Run Kafka Shell in a docker instance - to start a consumer
docker run --name docker-consumer --rm -v /var/run/docker.sock:/var/run/docker.sock \
-e HOST_IP=192.168.0.123 -i -t wurstmeister/kafka /bin/bash

KAFKA_HOME = /opt/kafka

# Start 'test-topic' consumer
$KAFKA_HOME/bin/kafka-console-consumer.sh --topic=test-topic \
--from-beginning --bootstrap-server `broker-list.sh`

# Delete topic data from 'test-topic'
$KAFKA_HOME/bin/kafka-topics.sh --delete --topic test-topic \
--bootstrap-server `broker-list.sh`