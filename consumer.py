# Import packages
from kafka import KafkaConsumer
from json import loads
import csv
import os.path

# Initialize Kafka Consumer
consumer = KafkaConsumer(
    'orders',
    bootstrap_servers=['localhost:32796',
                       'localhost:32797', 'localhost:32798'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    group_id='consumer-group-1',
    value_deserializer=lambda x: loads(x.decode('utf-8')))

# Set filename
filename = 'streaming-data.csv'

# Process messages
for message in consumer:
    message = message.value

    file_exists = os.path.isfile(filename)

    with open(filename, 'a', newline='') as file:
        fieldnames = ['order_id', 'product', 'quantity']
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        if not file_exists:
            writer.writeheader()

        writer.writerow(message)

    print("Order {} added to the data file\n".format(message['order_id']))
